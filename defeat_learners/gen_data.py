"""
template for generating data to fool learners (c) 2016 Tucker Balch
"""

import numpy as np

# this function should return a dataset (X and Y) that will work
# better for linear regression than decision trees
def best4LinReg(seed=908068):
    np.random.seed(seed)
    rows = np.random.randint(300,500)
    cols = np.random.randint(100,200)
    X = np.random.normal(np.random.randint(1,10),np.random.randint(1,10),(rows,cols))
    noise = np.random.standard_normal(rows)
    Y = np.sum(X * np.random.randint(1,10,cols), axis=1)  + noise
    Y = np.squeeze(Y)
    return X, Y

def best4DT(seed=908068):
    np.random.seed(seed)
    rows = 800
    cols = 5
    X = np.zeros((rows, cols))
    Y = np.zeros((rows))
    X[0:200] = np.random.normal(-10, 2,(200, cols))
    Y[0:200] = np.squeeze(np.sum(X[0:200] * np.random.randint(1,5,cols), axis=1))
    X[201:400] = np.random.normal(0, 0.25, (199, cols))
    Y[201:400] = np.squeeze(np.sum(X[201:400] * np.random.randint(6,10,cols), axis=1))
    X[401:600] = np.random.normal(500, 3, (199, cols))
    Y[401:600] = np.squeeze(np.sum(X[401:600] * np.random.randint(11,20,cols), axis=1))
    X[601:800] = np.random.normal(15, 0, (199, cols))
    Y[601:800] = 1
    Y = np.squeeze(Y)
    return X, Y

def author():
    return 'rll8' #Change this to your user ID

if __name__=="__main__":
    print(author())