"""
Name: Rodrigo De Luna Lara
GTID: rll8
"""

import numpy as np


class QLearner(object):

    def __init__(self, num_states=100, num_actions=4, alpha=0.2, gamma=0.9, rar=0.5, radr=0.99, dyna=0, verbose=False):
        self.verbose = verbose
        self.num_states = num_states
        self.num_actions = num_actions
        self.alpha = alpha
        self.gamma = gamma
        self.rar = rar
        self.radr = radr
        self.dyna = dyna
        self.s = 0
        self.a = 0
        self.r = np.zeros((num_states, num_actions))
        self.q = np.random.random((num_states, num_actions))
        self.t = np.zeros((num_states, num_actions, num_states))
        self.t_count = np.zeros((num_states, num_actions, num_states))

    def querysetstate(self, s):
        """
        @summary: Update the state without updating the Q-table
        @param s: The new state
        @returns: The selected action
        """
        self.s = s
        action = np.random.randint(0, self.num_actions)
        if np.random.random() > self.rar:
            action = np.argmax(self.q[s, :])
        return action

    def query(self, s_prime, r):
        """
        @summary: Update the Q table and return an action
        @param s_prime: The new state
        @param r: The new state
        @returns: The selected action
        """

        self.q[self.s, self.a] = (1 - self.alpha) * self.q[self.s, self.a] + \
                                 self.alpha * (r + self.gamma * np.max(self.q[s_prime, :]))

        if np.random.random() <= self.rar:
            action = np.random.randint(0, self.num_actions)
        else:
            action = np.argmax(self.q[s_prime, :])

        if self.dyna > 0:
            self.r[self.s, self.a] = (1 - self.alpha) * self.r[self.s, self.a] + (self.alpha * r)
            a_i = np.random.randint(0, self.num_actions, (self.dyna, 1))
            s_i = np.random.randint(0, self.num_states, (self.dyna, 1))
            s_prime_i = [np.argmax(np.random.multinomial(1, self.t[i[1], i[0], :])) for i in np.hstack((a_i, s_i))]
            for idx, row in enumerate(np.hstack((s_i, a_i))):
                self.q[row[0], row[1]] = (1 - self.alpha) * self.q[row[0], row[1]] + \
                                   self.alpha * (self.r[row[0], row[1]] + (self.gamma * self.q[s_prime_i[idx], np.argmax(self.q[s_prime_i[idx], :])]))
            self.t_count[self.s, self.a, s_prime] += 1
            self.t = np.divide(self.t_count, np.sum(self.t_count, axis=2, keepdims=True))

        self.s = s_prime
        self.a = action
        self.rar = self.rar * self.radr

        return action

    def author(self):
        return 'rll8'
