"""
Name: Rodrigo De Luna Lara
GTID: rll8
"""

import pandas as pd
import numpy as np
import datetime as dt
import os
from util import get_data


def author():
    return 'rll8'

def compute_portfolio_value(prices, allocs, value=1000000):
    # Normalize the prices
    norm_prices = prices / prices.iloc[0]
    # Multiply by the allocations
    alloc_prices = norm_prices * allocs
    # Multiply by the starting value to get the cash on each asset over time
    position_values = alloc_prices * value
    # Calculate the portfolio value
    portfolio_value = np.sum(position_values, axis=1)
    return portfolio_value

def compute_portfolio_stats(portfolio_value, sf=252.0, rfr=0.0):
    # Calculate the daily returns
    dr = (portfolio_value / portfolio_value.shift(1)) - 1
    # Calculate the average daily return
    adr = dr.mean()
    # Calculate the cumulative returns
    cr = (portfolio_value[-1] / portfolio_value[0]) - 1
    # Calculate the standard deviation of the daily returns
    sddr = dr.std()
    # Calculate the Sharpe ratio
    sr = np.sqrt(sf) * (adr - rfr)/sddr
    return cr, adr, sddr, sr

def compute_portvals(orders_file = "./orders/orders.csv", start_val = 1000000.0, commission=9.95, impact=0.005):
    # this is the function the autograder will call to test your code
    # NOTE: orders_file may be a string, or it may be a file object. Your
    # code should work correctly with either input
    if type(orders_file) == pd.core.frame.DataFrame or type(orders_file) == pd.core.series.Series:
        orders = orders_file
    elif type(orders_file) == str:
        orders = pd.read_csv(orders_file, index_col='Date', parse_dates=True, na_values=['nan'])
    orders.sort_index()

    symbols = orders['Symbol'].unique().tolist()

    start_date = orders.index.values[0]
    end_date = orders.index.values[-1]
    prices = get_data(symbols, pd.date_range(start_date, end_date))
    prices.fillna(method="ffill", inplace=True)
    prices.fillna(method="bfill", inplace=True)
    prices = prices.drop('SPY', 1)  # remove SPY

    trades = prices.copy()
    trades[:] = 0.0
    trades['Cash'] = 0.0

    for date, order in orders.iterrows():
        if order['Shares'] != 0:
            if order['Order'] == 'BUY':
                sign = -1
            if order['Order'] == 'SELL':
                sign = 1
            trades['Cash'][date] += (sign*order['Shares'] * prices[order['Symbol']][date]) \
                                    - (order['Shares'] * prices[order['Symbol']][date] * impact) \
                                    - commission
            trades[order['Symbol']][date] -= sign*order['Shares']

    trades['Cash'][0] += start_val
    holdings = trades.cumsum()
    holdings.iloc[:, :-1] *= prices
    portvals = holdings.sum(axis=1)

    return pd.DataFrame(portvals,columns=['Portfolio Value'])

def test_code():
    # this is a helper function you can use to test your code
    # note that during autograding his function will not be called.
    # Define input parameters

    of = "./orders/orders-02.csv"
    sv = 1000000

    # Process orders
    portvals = compute_portvals(orders_file = of, start_val = sv)
    if not isinstance(portvals, pd.DataFrame):
        raise TypeError("Portfolio Values Not a Dataframe")

    # Get portfolio stats
    # Here we just fake the data. you should use your code from previous assignments.
    cr, adr, sddr, sr = compute_portfolio_stats(portvals['Portfolio Value'])
    start_date = portvals.index.values[0]
    end_date = portvals.index.values[-1]

    # Get SPX portfolio stats
    prices_SPX = get_data(['$SPX'], pd.date_range(start_date, end_date))
    prices_SPX = prices_SPX.drop('SPY', 1)
    prices_SPX.fillna(method="ffill", inplace=True)
    prices_SPX.fillna(method="bfill", inplace=True)
    portvals_SPX = compute_portfolio_value(prices_SPX,[1],sv)
    cr_SPX, adr_SPX, sddr_SPX, sr_SPX = compute_portfolio_stats(portvals_SPX)


    # Compare portfolio against $SPX
    print "Date Range: {} to {}".format(start_date, end_date)
    print
    print "Sharpe Ratio of Fund: {}".format(sr)
    print "Sharpe Ratio of $SPX : {}".format(sr_SPX)
    print
    print "Cumulative Return of Fund: {}".format(cr)
    print "Cumulative Return of $SPX : {}".format(cr_SPX)
    print
    print "Standard Deviation of Fund: {}".format(sddr)
    print "Standard Deviation of $SPX : {}".format(sddr_SPX)
    print
    print "Average Daily Return of Fund: {}".format(adr)
    print "Average Daily Return of $SPX : {}".format(adr_SPX)
    print
    print "Final Portfolio Value: {}".format(portvals['Portfolio Value'].values[-1])

if __name__ == "__main__":
    test_code()
