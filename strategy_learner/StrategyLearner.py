"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch
Name: Rodrigo De Luna Lara
GTID: rll8
StrategyLearner
"""

import datetime as dt
import pandas as pd
import numpy as np
import util as ut
import QLearner as ql
import indicators as id


class StrategyLearner(object):

    # constructor
    def __init__(self, verbose=False, impact=0.0):
        self.verbose = verbose
        self.impact = impact
        self.learner = ql.QLearner(num_states=1000, num_actions=3, alpha=0.5, gamma=0.90, rar=0.7, radr=0.99, dyna=0)

    # this method should create a QLearner, and train it for trading
    def addEvidence(self, symbol="JPM", \
        sd=dt.datetime(2008,1,1), \
        ed=dt.datetime(2009,12,31), \
        sv = 100000):

        syms=[symbol]
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data(syms, dates)  # automatically adds SPY
        prices = prices_all[syms]  # only portfolio symbols
        prices_norm = prices / prices.iloc[0, :]

        sma_ratio = id.sma_ratio(prices_norm)
        bb_percent = id.calc_percentB(prices_norm)
        momentum = id.calc_std_momentum(prices_norm)

        discrete_sma_ratio, discrete_bb_percent, discrete_momentum = self.discretize(sma_ratio, bb_percent, momentum)

        indicators = np.char.add(np.char.add(np.squeeze(discrete_sma_ratio).astype(str), np.squeeze(discrete_bb_percent).astype(str)),np.squeeze(discrete_momentum).astype(str))
        indicators = indicators.astype(int)
        indicators = pd.DataFrame(indicators, index=prices_norm.index)

        i = 0
        maxIterations = 20
        cum_ret = np.zeros((maxIterations + 1))

        df_shares = pd.DataFrame(0, index=prices_norm.index, columns=['Shares'])
        df_orders = pd.DataFrame('NONE', index=prices_norm.index, columns=['Order'])
        df_symbols = pd.DataFrame(symbol, index=prices_norm.index, columns=['Symbol'])
        df_holdings = pd.DataFrame(0, index=prices_norm.index, columns=['Holdings'])
        df_portval = pd.DataFrame(0, index=prices_norm.index, columns=['PortVal'])
        df_portval.iloc[0] = sv
        df_cash = pd.DataFrame(0, index=prices_norm.index, columns=['Cash'])
        df_cash.iloc[0] = sv
        df_holdingval = pd.DataFrame(0, index=prices_norm.index, columns=['HoldingsVal'])
        df_trades_copy = pd.concat([df_symbols, df_orders, df_shares, df_holdings, df_portval, df_cash, df_holdingval], axis=1)

        while i < maxIterations:

            if i > 5:
                # if np.abs((cum_ret[i] - cum_ret[i-1])/cum_ret[i-1]) < 0.01:
                if (np.mean(cum_ret[i-5:i]) - cum_ret[i]) < 0.01:
                    break

            holdings = 0
            df_trades = df_trades_copy.copy()

            for index in range(1, prices_norm.shape[0]):

                index1 = df_trades.index[index]
                index2 = df_trades.index[index-1]
                index3 = df_trades.index[index-2]

                if index > 1:
                    reward = (df_trades.loc[index2, 'PortVal'] -
                              df_trades.loc[index3, 'PortVal'])
                else:
                    reward = 0

                action = self.learner.query(indicators.iloc[index], reward)

                if action == 0 and holdings < 1000:
                    df_trades.loc[index1, 'Order'] = 'BUY'
                    sign = 1
                elif action == 2 and holdings > -1000:
                    df_trades.loc[index1, 'Order'] = 'SELL'
                    sign = -1
                else:
                    sign = 0

                if sign != 0:
                    if holdings == 0:
                        df_trades.loc[index1, 'Shares'] = 1000
                        holdings += sign * 1000
                    else:
                        df_trades.loc[index1, 'Shares'] = 2000
                        holdings += sign * 2000
                else:
                    df_trades.loc[index1, 'Shares'] = 0

                df_trades.loc[index1, 'Holdings'] = holdings

                if df_trades.loc[index1, 'Shares'] != 0:
                    if df_trades.loc[index1, 'Order'] == 'BUY':
                        sign = -1
                    if df_trades.loc[index1, 'Order'] == 'SELL':
                        sign = 1
                    df_trades.loc[index1, 'Cash'] = df_trades.loc[index2, 'Cash'] + (sign * df_trades.loc[index1, 'Shares'] *
                                                    prices.loc[index1, symbol]) - \
                                                    (df_trades.loc[index1, 'Shares'] * prices.loc[index1, symbol] * self.impact)
                else:
                    df_trades.loc[index1, 'Cash'] = df_trades.loc[index2, 'Cash']

                df_trades.loc[index1, 'HoldingVal'] = df_trades.loc[index1, 'Holdings'] * prices.loc[index1, symbol]

                df_trades.loc[df_trades.index[index], 'PortVal'] = df_trades.loc[index1, 'Cash'] + \
                                                                   df_trades.loc[index1, 'HoldingVal']

            i += 1
            cum_ret[i] = (df_trades.loc[df_trades.index[-1], 'PortVal'] / sv) - 1
            if self.verbose:
                print("Cumulative return: " + str(cum_ret[i]))

    # this method should use the existing policy and test it against new data
    def testPolicy(self, symbol = "JPM", \
        sd=dt.datetime(2008,1,1), \
        ed=dt.datetime(2019,12,31), \
        sv = 100000):

        syms = [symbol]
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data(syms, dates)  # automatically adds SPY
        prices = prices_all[syms]  # only portfolio symbols

        prices_norm = prices / prices.iloc[0, :]

        sma_ratio = id.sma_ratio(prices_norm)
        bb_percent = id.calc_percentB(prices_norm)
        momentum = id.calc_std_momentum(prices_norm)

        discrete_sma_ratio, discrete_bb_percent, discrete_momentum = self.discretize(sma_ratio, bb_percent, momentum)

        indicators = np.char.add(np.char.add(np.squeeze(discrete_sma_ratio).astype(str), np.squeeze(discrete_bb_percent).astype(str)), np.squeeze(discrete_momentum).astype(str))
        indicators = indicators.astype(int)
        indicators = pd.DataFrame(indicators, index=prices_norm.index)

        df_shares = pd.DataFrame(0, index=prices_norm.index, columns=['Shares'])
        df_orders = pd.DataFrame('NONE', index=prices_norm.index, columns=['Order'])
        df_symbols = pd.DataFrame(symbol, index=prices_norm.index, columns=['Symbol'])
        df_trades = pd.concat([df_symbols, df_orders, df_shares],axis=1)

        holdings = 0
        position = 0
        initial_state = indicators[0] + position*1000
        self.learner.querysetstate(initial_state)

        for index, row in prices_norm.iterrows():
            action = self.learner.querysetstate(indicators.loc[index] + position*1000)
            if action == 0 and holdings < 1000:
                df_trades.loc[index, 'Order'] = 'BUY'
                sign = 1
            elif action == 2 and holdings > -1000:
                df_trades.loc[index, 'Order'] = 'SELL'
                sign = -1
            else:
                sign = 0

            if sign != 0:
                if holdings == 0:
                    df_trades.loc[index, 'Shares'] = sign * 1000
                    holdings += sign * 1000
                else:
                    df_trades.loc[index, 'Shares'] = sign * 2000
                    holdings += sign * 2000
            else:
                df_trades.loc[index, 'Shares'] = 0

        return df_trades.drop(['Symbol', 'Order'], axis=1)

    def discretize(self, sma_ratio, bb_percent, momentum):
        discrete_sma_ratio = np.digitize(sma_ratio, np.arange(-0.5, 0.5, 0.1)) - 1
        discrete_bb_percent = np.digitize(bb_percent, np.arange(-1, 1, 0.2)) - 1
        discrete_momentum = np.digitize(momentum, np.arange(-10, 10, 2)) - 1
        return discrete_sma_ratio, discrete_bb_percent, discrete_momentum

    def author(self):
        return 'rll8'
    

if __name__=="__main__":
    sl = StrategyLearner(verbose=True)
    sl.addEvidence()
    sl.testPolicy()

