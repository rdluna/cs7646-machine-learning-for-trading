"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch
Name: Rodrigo De Luna Lara
GTID: rll8
Experiment 1: JPM Benchmark Analysis
"""

import datetime as dt
import numpy as np
import marketsimcode as marketsim
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import ManualStrategy as ms
import StrategyLearner as slr

def author():
    return 'rll8'

if __name__ == "__main__":
    portvals_norm_train = np.zeros((505, 50))
    for idx in range(50):
        sl = slr.StrategyLearner()
        sl.addEvidence(symbol='JPM', sd=dt.datetime(2008, 1, 1), ed=dt.datetime(2009, 12, 31))
        df_trades_train = sl.testPolicy(symbol='JPM', sd=dt.datetime(2008, 1, 1), ed=dt.datetime(2009, 12, 31))
        df_trades_train['Symbol'] = 'JPM'
        df_trades_train['Order'] = 'BUY'
        df_trades_train.loc[df_trades_train['Shares'] < 0, 'Order'] = 'SELL'
        df_trades_train.loc[df_trades_train['Shares'] == 0, 'Order'] = 'NONE'
        portvals_train = marketsim.compute_portvals(df_trades_train)
        portvals_train = portvals_train['Portfolio Value']
        portvals_norm_train[:, idx] = portvals_train / portvals_train.iloc[0]

    manual_strategy, benchmark = ms.main()

    plt.figure()
    plt.plot_date(portvals_train.index, np.median(portvals_norm_train, axis=1), marker=None, ls='-', color='black',
                  label='Q-Learner Median')
    plt.fill_between(portvals_train.index, np.min(portvals_norm_train, axis=1), np.max(portvals_norm_train, axis=1),
                     color='grey', alpha=0.5, label='Q-Learner Bounds')
    plt.plot_date(portvals_train.index, manual_strategy, marker=None, ls='-', color='blue', label='Manual Strategy')
    plt.plot_date(portvals_train.index, benchmark, marker=None, ls='-', color='red', label='Benchmark')
    plt.grid(True)
    plt.xlim([dt.datetime(2008, 1, 1), dt.datetime(2009, 12, 31)])
    plt.ylabel('Norm. Portfolio Value')
    plt.xlabel('Date (mm/dd/yy)')
    ax = plt.gca()
    ax.xaxis.set_major_locator(mticker.LinearLocator(6))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    plt.ylim([-1.75, 3.5])
    plt.yticks(np.arange(-1.75, 4, 0.5))
    ax.yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    plt.legend(loc='upper left')
    plt.suptitle('Strategy Learner')
    plt.title('50 Learner Test')
    plt.savefig("/home/rll8/strategy_learner/plot1.png")
    plt.close()
