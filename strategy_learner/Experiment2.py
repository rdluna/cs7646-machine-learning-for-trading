"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch
Name: Rodrigo De Luna Lara
GTID: rll8
Experiment 2: Impact Analysis
"""

import datetime as dt
import numpy as np
import marketsimcode as marketsim
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import StrategyLearner as slr
import ManualStrategy as ms

def author():
    return 'rll8'

if __name__ == "__main__":
    impacts = np.arange(0, 0.225, 0.025)
    iterations = 5
    portvals_norm_train = np.zeros((505, len(impacts), iterations))
    n_trades = np.zeros((len(impacts), iterations))
    cum_ret = np.zeros((len(impacts), iterations))
    sv = 100000
    for idx in range(len(impacts)):
        for it in range(iterations):
            sl = slr.StrategyLearner(impact=impacts[idx])
            sl.addEvidence(symbol='JPM', sd=dt.datetime(2008, 1, 1), ed=dt.datetime(2009, 12, 31), sv=sv)
            df_trades_train = sl.testPolicy(symbol='JPM', sd=dt.datetime(2008, 1, 1), ed=dt.datetime(2009, 12, 31))
            df_trades_train['Symbol'] = 'JPM'
            df_trades_train['Order'] = 'BUY'
            df_trades_train.loc[df_trades_train['Shares'] < 0, 'Order'] = 'SELL'
            df_trades_train.loc[df_trades_train['Shares'] == 0, 'Order'] = 'NONE'
            n_trades[idx, it] = df_trades_train[df_trades_train['Shares'] != 0].shape[0]
            portvals_train = marketsim.compute_portvals(df_trades_train)
            portvals_train = portvals_train['Portfolio Value']
            cum_ret[idx, it] = (portvals_train.iloc[-1] / sv) - 1
            portvals_norm_train[:, idx, it] = portvals_train / portvals_train.iloc[0]

    plt.figure()
    plt.plot(impacts, np.median(n_trades, axis=1), marker='None', ls='-', color="#053061", label='Median Trades')
    plt.fill_between(impacts, np.min(n_trades, axis=1), np.max(n_trades, axis=1), color='grey', alpha=0.5, label='Bounds')
    plt.grid(True)
    plt.legend()
    plt.ylabel('Number of Trades')
    plt.xlabel('Impact')
    plt.xlim([0, 0.2])
    plt.xticks(impacts)
    plt.ylim([0, 120])
    plt.yticks(np.arange(0, 140, 20))
    plt.title('Number of Trades vs Impact')
    plt.savefig('/home/rll8/strategy_learner/plot2.png')
    plt.close()

    manual_strategy, benchmark = ms.main()

    plt.figure()
    mpl.rcParams['axes.color_cycle'] = ['#40004b', '#762a83', '#9970ab', '#c2a5cf', '#e7d4e8', '#d9f0d3', '#a6dba0',
                                        '#5aae61', '#1b7837', '#00441b']
    p1 = plt.plot_date(portvals_train.index, np.median(portvals_norm_train, axis=2), marker='None', ls='-')
    p2 = plt.plot_date(portvals_train.index, manual_strategy, marker=None, ls='-', color='blue',
                       label='Manual Strategy')
    p3 = plt.plot_date(portvals_train.index, benchmark, marker=None, ls='-', color='red', label='Benchmark')
    l1 = plt.legend(p1, [str(i) for i in impacts], title='Impact', loc=0, ncol=2, fontsize=10)
    l2 = plt.legend((p2, p3), labels=('Manual Strategy', 'Benchmark'), loc=3)
    plt.gca().add_artist(l1)
    plt.ylim([-1.75, 3.5])
    plt.yticks(np.arange(-1.75, 4, 0.5))
    plt.ylabel('Norm. Portfolio Value')
    plt.xlabel('Date (mm/dd/yy)')
    plt.grid(True)
    ax = plt.gca()
    ax.xaxis.set_major_locator(mticker.LinearLocator(6))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    plt.xlim([dt.datetime(2008, 1, 1), dt.datetime(2009, 12, 31)])
    plt.title('Impact vs Performance')
    for idx, l in enumerate(l2.get_lines()):
        if idx == 0:
            l.set_color('blue')
        if idx == 1:
            l.set_color('red')
    plt.savefig('/home/rll8/strategy_learner/plot3.png')
    plt.close()

