from util import get_data
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import datetime as dt
import marketsimcode as marketsim
import indicators


def testPolicy(symbol = "AAPL", sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 100000):
    data = get_data([symbol], pd.date_range(sd,ed))
    data.bfill(inplace=True)
    data.ffill(inplace=True)
    data = data/data.iloc[0,:]
    data = data[symbol]

    # Get Percent B
    percentB = indicators.calc_percentB(data)
    percentB_buy = pd.DataFrame(percentB < 0.03, index = data.index)
    percentB_sell = pd.DataFrame(percentB > 1.03, index = data.index)

    # Get Momentum
    momentum = indicators.calc_std_momentum(data)
    momentum = momentum.iloc[20:]
    zero_crossing_momentum = momentum.index.values[1:][np.diff(np.sign(momentum.values)) != 0]
    momentum_buy = percentB_buy.copy()
    momentum_buy.values[:] =  False
    momentum_sell = percentB_sell.copy()
    momentum_sell.values[:] = False
    for date in zero_crossing_momentum:
        idx = np.where(momentum.index.values == date)[0]
        if momentum.values[idx - 1] < momentum.values[idx]:
            momentum_buy.loc[date] = True
        if momentum.values[idx - 1] > momentum.values[idx]:
            momentum_sell.loc[date] = True

    # Get SMA ratio
    sma_ratio = indicators.sma_ratio(data)
    sma_ratio = sma_ratio[50:]
    zero_crossing_sma = sma_ratio.index.values[1:][np.diff(np.sign(sma_ratio.values)) != 0]
    sma_buy = momentum_buy.copy()
    sma_buy.values[:] = False
    sma_sell = momentum_sell.copy()
    sma_sell.values[:] = False
    for date in zero_crossing_sma:
        idx = np.where(sma_ratio.index.values == date)[0]
        if sma_ratio.values[idx - 1] < sma_ratio.values[idx]:
            sma_buy.loc[date] = True
        if sma_ratio.values[idx - 1] > sma_ratio.values[idx]:
            sma_sell.loc[date] = True

    buy_dates = data.index.values[np.squeeze(percentB_buy | momentum_buy | sma_buy)]
    sell_dates = data.index.values[np.squeeze(percentB_sell | momentum_sell | sma_sell)]

    invalid_dates = np.intersect1d(buy_dates,sell_dates)
    buy_dates = np.setdiff1d(buy_dates,invalid_dates)
    sell_dates = np.setdiff1d(sell_dates,invalid_dates)

    orders = pd.DataFrame(index=data.index, columns=['Order'])
    orders['Order'] = 'NONE'
    orders['Order'][buy_dates] = 'BUY'
    orders['Order'][sell_dates] = 'SELL'

    df_trades = pd.DataFrame(index=data.index, columns=['Shares','Order','Holdings','Trades','Symbol'])
    holdings = 0
    max_shares = 1000

    for idx in range(data.shape[0]):
        # Iterate until the second to last day
        if data.index[idx] != data.index[-1]:
            order = orders['Order'].values[idx]
            if order == 'BUY' and holdings != 1000:
                sign = 1
            elif order == 'SELL' and holdings != -1000:
                sign = -1
            else:
                order = 'NONE'
                sign = 0
            if order != 'NONE':
                if holdings == 0:
                    df_trades['Shares'][idx] = max_shares
                    holdings += sign*max_shares
                elif abs(holdings):
                    df_trades['Shares'][idx] = 2*max_shares
                    holdings += 2*sign*max_shares
            else:
                df_trades['Shares'][idx] = 0
            df_trades['Order'][idx] = order
            df_trades['Holdings'][idx] = holdings
            df_trades['Trades'][idx] = df_trades['Shares'][idx]*sign
            df_trades['Symbol'][idx] = symbol

    df_trades.dropna(inplace=True)
    df_trades = df_trades[df_trades['Order'] != 'NONE']
    #return df_trades[['Symbol','Order','Shares']]
    return df_trades['Trades']



if __name__ == "__main__":
    """
       IN SAMPLE
    """
    sd = dt.datetime(2008,1,1)
    ed = dt.datetime(2009,12,31)
    sv = 100000
    trades = testPolicy('JPM',sd,ed,sv)

    trades_df = pd.DataFrame(index=trades.index)
    trades_df['Symbol'] = 'JPM'
    trades_df['Shares'] = np.abs(trades.values)
    trades_df['Order'] = ''
    trades_df['Order'][np.sign(trades) == 1] = 'BUY'
    trades_df['Order'][np.sign(trades) == -1] = 'SELL'

    portvals = marketsim.compute_portvals(trades_df, sv)
    portvals = portvals['Portfolio Value']

    prices = get_data(['JPM'], pd.date_range(sd, ed))
    prices = prices['JPM']
    prices.bfill(inplace=True)
    prices.ffill(inplace=True)

    portvals = portvals.reindex(prices.index)
    portvals.ffill(inplace=True)
    portvals.bfill(inplace=True)

    portvals_benchmark = portvals * 0 + (sv - 1000 * prices[0]) - 9.95 - (1000 * prices[0])*0.005
    portvals_benchmark = portvals_benchmark + 1000 * prices

    portvals_norm = portvals / portvals.iloc[0]
    portvals_benchmark_norm = portvals_benchmark / portvals_benchmark.iloc[0]

    cr, adr, sddr, sr = marketsim.compute_portfolio_stats(portvals)
    cr_benchmark, adr_benchmark, sddr_benchmark, sr_benchmark = marketsim.compute_portfolio_stats(portvals_benchmark)
    in_sample_table = np.array(((cr, adr, sddr, sr),(cr_benchmark, adr_benchmark, sddr_benchmark, sr_benchmark)))
    np.savetxt("/home/rll8/manual_strategy/in_sample.csv", in_sample_table, fmt='%0.6f', delimiter=',')


    plt.figure()
    plt.plot_date(portvals_norm.index, portvals_norm, marker=None, ls='-', color='black', label='Portfolio')
    plt.plot_date(portvals_benchmark_norm.index, portvals_benchmark_norm, marker=None, ls='-', color='blue',label='Benchmark')
    [plt.axvline(line, lw=0.5, color='green') for line in trades[trades_df['Order'] == 'BUY'].index.values]
    [plt.axvline(line, lw=0.5, color='red') for line in trades[trades_df['Order'] == 'SELL'].index.values]
    plt.grid(True)
    plt.xlim([sd, ed])
    plt.ylabel('Norm. Portfolio Value')
    plt.xlabel('Date (mm/dd/yy)')
    ax = plt.gca()
    ax.xaxis.set_major_locator(mticker.LinearLocator(6))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    plt.ylim([0.5, 1.5])
    plt.yticks(np.arange(0.5, 1.625, 0.125))
    ax.yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    plt.legend(loc='upper left')
    plt.suptitle('Manual Strategy')
    plt.title('In Sample Period', fontsize=10)
    plt.savefig("/home/rll8/manual_strategy/plot5.png")
    plt.close()

    """
    OUT OF SAMPLE
    """

    sd = dt.datetime(2010, 1, 1)
    ed = dt.datetime(2011, 12, 31)
    sv = 100000
    trades = testPolicy('JPM', sd, ed, sv)

    trades_df = pd.DataFrame(index=trades.index)
    trades_df['Symbol'] = 'JPM'
    trades_df['Shares'] = np.abs(trades.values)
    trades_df['Order'] = ''
    trades_df['Order'][np.sign(trades) == 1] = 'BUY'
    trades_df['Order'][np.sign(trades) == -1] = 'SELL'

    portvals = marketsim.compute_portvals(trades_df, sv)
    portvals = portvals['Portfolio Value']

    prices = get_data(['JPM'], pd.date_range(sd, ed))
    prices = prices['JPM']
    prices.bfill(inplace=True)
    prices.ffill(inplace=True)

    portvals = portvals.reindex(prices.index)
    portvals.ffill(inplace=True)
    portvals.bfill(inplace=True)

    portvals_benchmark = portvals * 0 + (sv - 1000 * prices[0]) - 9.95 - (1000 * prices[0])*0.005
    portvals_benchmark = portvals_benchmark + 1000 * prices

    portvals_norm = portvals / portvals.iloc[0]
    portvals_benchmark_norm = portvals_benchmark / portvals_benchmark.iloc[0]

    cr, adr, sddr, sr = marketsim.compute_portfolio_stats(portvals)
    cr_benchmark, adr_benchmark, sddr_benchmark, sr_benchmark = marketsim.compute_portfolio_stats(portvals_benchmark)
    out_sample_table = np.array(((cr, adr, sddr, sr), (cr_benchmark, adr_benchmark, sddr_benchmark, sr_benchmark)))
    np.savetxt("/home/rll8/manual_strategy/out_sample.csv", out_sample_table, fmt='%0.6f', delimiter=',')

    plt.figure()
    plt.plot_date(portvals_norm.index, portvals_norm, marker=None, ls='-', color='black', label='Portfolio')
    plt.plot_date(portvals_benchmark_norm.index, portvals_benchmark_norm, marker=None, ls='-', color='blue',label='Benchmark')
    plt.grid(True)
    plt.xlim([sd, ed])
    plt.ylabel('Norm. Portfolio Value')
    plt.xlabel('Date (mm/dd/yy)')
    ax = plt.gca()
    ax.xaxis.set_major_locator(mticker.LinearLocator(6))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    plt.ylim([0.5, 1.5])
    plt.yticks(np.arange(0.5, 1.625, 0.125))
    ax.yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    plt.legend(loc='upper left')
    plt.suptitle('Manual Strategy')
    plt.title('Out of Sample Period', fontsize=10)
    plt.savefig("/home/rll8/manual_strategy/plot6.png")
    plt.close()