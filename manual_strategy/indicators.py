from util import get_data
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import datetime as dt

def bollinger_bands(data, window=20):
    moving_average = data.rolling(window).mean()
    moving_std = data.rolling(window).std()
    bands = pd.DataFrame(index=data.index)
    bands['upper'] = moving_average + moving_std*2
    bands['lower'] = moving_average - moving_std*2
    return bands

def calc_percentB(data, bbands = None, window=20):
    if bbands is None:
        bbands = bollinger_bands(data, window)
    if len(data.shape) == 1:
        return (data.values - bbands['lower'].values) / (bbands['upper'].values - bbands['lower'].values)
    elif data.shape[1] == 1:
        return (np.squeeze(data.values) - bbands['lower'].values) / (bbands['upper'].values - bbands['lower'].values)


def calc_std_momentum(data, window=20):
    momentum = data/data.shift(window)
    momentum = (momentum - momentum.mean())/momentum.std()
    return momentum

def sma_ratio(data, window1=20, window2=50):
    sma_20 = data.rolling(window1).mean()
    sma_50 = data.rolling(window2).mean()
    return sma_20/sma_50 - 1

def calc_std_volatility(data, window=20):
    daily_returns = (data / data.shift(1)) - 1
    volatility = daily_returns.rolling(window).std()
    volatility = (volatility - volatility.mean())/volatility.std()
    return volatility

if __name__ == "__main__":
    sd_train = dt.datetime(2008,1,1)
    ed_train = dt.datetime(2009,12,31)
    sd_test = dt.datetime(2010,1,1)
    ed_test = dt.datetime(2011,12,31)
    starting_value = 100000
    symbols = ['JPM']
    window = 20

    train_data = get_data(symbols,pd.date_range(sd_train, ed_train))
    train_data.ffill(inplace=True)
    train_data.bfill(inplace=True)
    train_data = train_data/train_data.iloc[0,:]

    """
    Bollinger Bands
    """

    bbands = bollinger_bands(train_data[symbols])
    bbratio = calc_percentB(train_data[symbols], bbands)

    fig, axarr = plt.subplots(2, sharex=True)
    axarr[0].plot_date(train_data.index.values, train_data[symbols].values, marker=None, ls='-', color='blue')
    axarr[0].plot_date(train_data.index.values, train_data[symbols].rolling(window).mean(), marker=None, ls='--', color='red')
    axarr[0].fill_between(train_data.index.values, bbands['lower'].values, bbands['upper'].values, color='grey', alpha=0.5)
    axarr[1].plot_date(bbands.index.values, bbratio, marker=None, ls='-', color='red')
    axarr[1].axhline(0.03, color='green')
    axarr[1].axhline(1.03, color='green')
    axarr[0].grid(True)
    axarr[0].legend(['JPM','MA20'])
    axarr[0].set_xlim([sd_train, ed_train])
    axarr[0].set_ylabel('Norm. Adj. Close Price')
    axarr[0].xaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[0].xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    axarr[0].yaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[0].yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    axarr[1].grid(True)
    axarr[1].set_xlim([sd_train, ed_train])
    axarr[1].set_ylabel('Percent B')
    axarr[1].set_xlabel('Date (mm/dd/yy)')
    axarr[1].xaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[1].xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    axarr[1].yaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[1].yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    axarr[0].set_title('Indicator 1: Bollinger Bands (%B)')
    plt.savefig("/home/rll8/manual_strategy/plot1.png")
    plt.close()

    """
    Momentum
    """

    momentum = calc_std_momentum(train_data[symbols])

    fig, axarr = plt.subplots(2, sharex=True)
    axarr[0].plot_date(train_data.index.values, train_data[symbols].values, marker=None, ls='-', color='blue')
    axarr[1].plot_date(train_data.index.values, momentum.values, marker=None, ls='-', color='red')
    axarr[1].axhline(0, color='green')
    axarr[0].grid(True)
    axarr[0].legend(['JPM'])
    axarr[0].set_xlim([sd_train, ed_train])
    axarr[0].set_ylabel('Norm. Adj. Close Price')
    axarr[0].xaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[0].xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    axarr[0].yaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[0].yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    axarr[1].grid(True)
    axarr[1].set_ylabel('Std. Momentum')
    axarr[1].xaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[1].xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    axarr[1].yaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[1].yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    axarr[1].set_xlabel('Date (mm/dd/yy)')
    axarr[0].set_title('Indicator 2: Momentum Crossover')
    plt.savefig("/home/rll8/manual_strategy/plot2.png")
    plt.close()

    """
    SMA Ratio
    """

    sma = sma_ratio(train_data[symbols])

    fig, axarr = plt.subplots(2, sharex=True)
    axarr[0].plot_date(train_data.index.values, train_data[symbols].values, marker=None, ls='-', color='blue')
    axarr[0].plot_date(train_data.index.values, train_data[symbols].rolling(20).mean(), marker=None, ls='--', color='red')
    axarr[0].plot_date(train_data.index.values, train_data[symbols].rolling(50).mean(), marker=None, ls='--',color='black')
    axarr[1].plot_date(train_data.index.values, sma, marker=None, ls='-',color='red')
    axarr[1].axhline(0, color='green')
    axarr[0].grid(True)
    axarr[0].legend(['JPM', 'MA20', 'MA50'])
    axarr[0].set_xlim([sd_train, ed_train])
    axarr[0].set_ylabel('Norm. Adj. Close Price')
    axarr[0].xaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[0].xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    axarr[0].yaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[0].yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    axarr[1].grid(True)
    axarr[1].set_ylabel('SMA Ratio')
    axarr[1].xaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[1].xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    axarr[1].yaxis.set_major_locator(mticker.LinearLocator(6))
    axarr[1].yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    axarr[1].set_xlabel('Date (mm/dd/yy)')
    axarr[0].set_title('Indicator 3: SMA Ratio Crossover')
    plt.savefig("/home/rll8/manual_strategy/plot3.png")
    plt.close()