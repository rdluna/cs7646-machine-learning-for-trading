from util import get_data
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import datetime as dt
import marketsimcode as marketsim

def testPolicy(symbol = "AAPL", sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 100000):
    data = get_data([symbol], pd.date_range(sd,ed))
    data.bfill(inplace=True)
    data.ffill(inplace=True)
    data = data/data.iloc[0,:]
    data_benchmark = data['SPY']
    data = data[symbol]

    df_trades = pd.DataFrame(index=data.index, columns=['Shares','Order','Holdings','Trades','Symbol'])
    holdings = 0
    max_shares = 1000

    for idx in range(data.shape[0]):
        # Iterate until the second to last day
        if data.index[idx] != data.index[-1]:
            if (data[idx+1] > data[idx]) and (holdings < max_shares):
                order = 'BUY'
                sign = 1
            elif (data[idx+1] < data[idx]) and (holdings > -max_shares):
                order = 'SELL'
                sign = -1
            else:
                order = 'NONE'
            if order != 'NONE':
                if holdings == 0:
                    df_trades['Shares'][idx] = max_shares
                    holdings += sign*max_shares
                else:
                    df_trades['Shares'][idx] = 2*max_shares
                    holdings += 2*sign*max_shares
            else:
                df_trades['Shares'][idx] = 0
                sign = 0
            df_trades['Order'][idx] = order
            df_trades['Holdings'][idx] = holdings
            df_trades['Trades'][idx] = df_trades['Shares'][idx]*sign
            df_trades['Symbol'][idx] = symbol

    df_trades.dropna(inplace=True)
    df_trades = df_trades[df_trades['Order'] != 'NONE']
    #return df_trades[['Symbol','Order','Shares']]
    return df_trades['Trades']

if __name__ == "__main__":
    sd = dt.datetime(2008,1,1)
    ed = dt.datetime(2009,12,31)
    sv = 100000
    trades = testPolicy('JPM',sd,ed,sv)

    trades_df = pd.DataFrame(index=trades.index)
    trades_df['Symbol'] = 'JPM'
    trades_df['Shares'] = np.abs(trades.values)
    trades_df['Order'] = ''
    trades_df['Order'][np.sign(trades) == 1] = 'BUY'
    trades_df['Order'][np.sign(trades) == -1] = 'SELL'

    portvals = marketsim.compute_portvals(trades_df, sv, 0, 0)
    portvals = portvals['Portfolio Value']

    prices = get_data(['JPM'], pd.date_range(sd,ed))
    prices = prices['JPM']
    prices.bfill(inplace=True)
    prices.ffill(inplace=True)

    portvals = portvals.reindex(prices.index)
    portvals.ffill(inplace=True)
    portvals.bfill(inplace=True)

    portvals_benchmark = portvals * 0 + (sv - 1000*prices[0])
    portvals_benchmark = portvals_benchmark + 1000*prices

    portvals_norm = portvals / portvals.iloc[0]
    portvals_benchmark_norm = portvals_benchmark / portvals_benchmark.iloc[0]

    cr, adr, sddr, sr = marketsim.compute_portfolio_stats(portvals)
    cr_benchmark, adr_benchmark, sddr_benchmark, sr_benchmark = marketsim.compute_portfolio_stats(portvals_benchmark)
    in_sample_table = np.array(((cr, adr, sddr, sr), (cr_benchmark, adr_benchmark, sddr_benchmark, sr_benchmark)))
    np.savetxt("/home/rll8/manual_strategy/best_in_sample.csv", in_sample_table, fmt='%0.6f', delimiter=',')

    plt.figure()
    plt.plot_date(portvals_norm.index, portvals_norm, marker=None, ls='-', color='black', label='Portfolio')
    plt.plot_date(portvals_benchmark_norm.index, portvals_benchmark_norm, marker=None, ls='-', color='blue', label='Benchmark')
    plt.grid(True)
    plt.xlim([sd, ed])
    plt.ylabel('Norm. Portfolio Value')
    plt.xlabel('Date (mm/dd/yy)')
    ax = plt.gca()
    ax.xaxis.set_major_locator(mticker.LinearLocator(6))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
    plt.ylim([0,7])
    plt.yticks(np.arange(0,8))
    ax.yaxis.set_major_formatter(mticker.FormatStrFormatter('%.2f'))
    plt.legend(loc = 'upper left')
    plt.suptitle('Best Strategy')
    plt.title('In Sample Period', fontsize=10)
    plt.savefig("/home/rll8/manual_strategy/plot4.png")
    plt.close()