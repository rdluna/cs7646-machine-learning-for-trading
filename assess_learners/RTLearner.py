import numpy as np
import pandas as pd

class RTLearner(object):
    def __init__(self, leaf_size=1, verbose=False):
        self.leaf_size = leaf_size
        self.verbose = verbose
        self.nodes = []
        self.node_num = 0

    def author(self):
        return 'rll8'

    def addEvidence(self, Xtrain, Ytrain):
        self.buildTree(Xtrain, Ytrain)
        self.tree = sorted(self.nodes)
        if self.verbose:
            print(self.tree)

    def buildTree(self, Xtrain, Ytrain):
        """
        Method to build a random regression tree using the ID3 algorithm
        :param Xtrain: features matrix (ndarray)
        :param Ytrain: response matrix (ndarray)
        :return node.num: this method returns the current node index
        """
        self.node_num += 1
        # If there is only 1 observation left or all y-values are the same assign the y-value to the leaf node
        if Xtrain.shape[0] == 1 or all(Ytrain == Ytrain[0]):
            self.nodes.append((self.node_num, 'LEAF', Ytrain[0], float('nan'), float('nan')))
        # If the number of points is less than or equal to the leaf size, aggregate the points into the leaf node
        elif Xtrain.shape[0] <= self.leaf_size:
            self.nodes.append((self.node_num, 'LEAF', np.mean(Ytrain), float('nan'), float('nan')))
        # Else build the tree as usual
        else:
            currentNode = self.node_num
            # Get columns where values are different
            allNotEqual = np.where(~np.all(Xtrain == Xtrain[0,:], axis=0))[0]
            # Get a random integer to select a feature
            randSplit = allNotEqual[int(np.random.randint(0, allNotEqual.size, 1))]
            # Get the value to split on the current node with the median
            splitVal = (Xtrain[np.random.randint(0, Xtrain.shape[0], 1), randSplit] +
                        Xtrain[np.random.randint(0, Xtrain.shape[0], 1), randSplit])/2
            # If the split value is 0 use the mean instead of the median
            if splitVal == 0:
                splitVal = np.nanmean(Xtrain[:, randSplit])
            # If the split value is equal to the maximum of the data, use the mean
            if splitVal == np.max(Xtrain[:, randSplit]):
                splitVal = np.nanmean(Xtrain[:, randSplit])
            # Go on building on the left
            leftNode  = self.buildTree(Xtrain[Xtrain[:, randSplit] <= splitVal], Ytrain[Xtrain[:, randSplit] <= splitVal])
            # Go on building on the right
            rightNode = self.buildTree(Xtrain[Xtrain[:, randSplit] > splitVal], Ytrain[Xtrain[:, randSplit] > splitVal])
            # Append the current node to the tree structure
            self.nodes.append((currentNode, randSplit, splitVal, currentNode+1, leftNode+1))
        # Return the current node number
        return self.node_num

    def evalTree(self, Xtest, node, idx):
        """
        Method to predict based on the trained tree
        :param Xtest: features to predict on (ndarray)
        :param node: index of the node being evaluated
        :param idx: indices of the features in the current bag with respect to Xtest
        :return: this method doesn't return anything
        """
        # The nodes in the tree description are 1-indexed
        node -= 1
        # If the current node is a LEAF node then return its value as predicted value
        if self.tree[node][1] == 'LEAF':
            self.Ytest[idx] = self.tree[node][2]
        # If the current node is not a LEAF node then split the data and pass it down the tree
        else:
            # Get the index of the variable used for splitting
            splitIdx = self.tree[node][1]
            # Get the splitting value
            splitVal = self.tree[node][2]
            # Build a boolean array to distinguish between data that goes to the left and data that goes to the right
            split = Xtest[:, splitIdx] <= splitVal
            # Get the indices of the data going to the left
            leftIdx = idx[np.where(split)]
            # Get the subset of data going to the left
            leftX = Xtest[split, :]
            # If there is data then continue going down the tree on the left node
            if leftX.shape[0] > 0:
                self.evalTree(leftX, self.tree[node][3], leftIdx)
            # Get the indices of the data going to the right
            rightIdx = idx[np.where(~split)]
            # Get the subset of data going to the right
            rightX = Xtest[~split, :]
            # If there is data then continue going down the tree on the right node
            if rightX.shape[0] > 0:
                self.evalTree(rightX, self.tree[node][4], rightIdx)

    def query(self, Xtest):
        self.Xtest = Xtest
        self.Ytest = np.ones((Xtest.shape[0], 1)) * float('nan')
        self.evalTree(Xtest, 1, np.arange(Xtest.shape[0]))
        return np.squeeze(self.Ytest)


if __name__ == "__main__":
    data = pd.read_csv("/home/rll8/assess_learners/Data/Istanbul.csv", index_col=0)
    Xtrain = data.values[:, 0:-1]
    Ytrain = data.values[:, -1]
    learner = RTLearner()
    learner.addEvidence(Xtrain, Ytrain)
    Y = learner.query(Xtrain)