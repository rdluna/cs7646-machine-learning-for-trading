"""
Test a learner.  (c) 2015 Tucker Balch
"""

import numpy as np
import math
import DTLearner as dt
import RTLearner as rt
import BagLearner as bl
import pandas as pd
import time

if __name__=="__main__":

    files = ['Istanbul.csv']

    for file in files:
        if file != "Istanbul.csv":
            data = pd.read_csv("/home/rll8/assess_learners/Data/" + file).values
        else:
            data = pd.read_csv("/home/rll8/assess_learners/Data/" + file, index_col=0).values

        # compute how much of the data is training and testing
        train_rows = int(0.6* data.shape[0])
        test_rows = data.shape[0] - train_rows

        # separate out training and testing data
        trainX = data[:train_rows,0:-1]
        trainY = data[:train_rows,-1]
        testX = data[train_rows:,0:-1]
        testY = data[train_rows:,-1]

        trainRMSE = []
        trainCorr = []
        testRMSE = []
        testCorr = []

        if False:
            # Train decision tree with varying leaf size
            for leaf_size in range(trainX.shape[0]):
                print(leaf_size + 1)
                learner = dt.DTLearner(leaf_size=int(leaf_size+1)) # create a LinRegLearner
                learner.addEvidence(trainX, trainY) # train it

                # evaluate in sample
                predY = learner.query(trainX) # get the predictions
                trainRMSE.append(math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0]))
                trainCorr.append(np.corrcoef(np.squeeze(predY), y=trainY)[0,1])

                # evaluate out of sample
                predY = learner.query(testX) # get the predictions
                testRMSE.append(math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0]))
                testCorr.append(np.corrcoef(np.squeeze(predY), y=testY)[0,1])

            np.savetxt("/home/rll8/assess_learners/Report Data/DTLearner.csv",
                       np.stack((np.array(trainRMSE), np.array(trainCorr), np.array(testRMSE), np.array(testCorr))).transpose(),
                       fmt='%.8f', delimiter=',')

        trainRMSE = []
        trainCorr = []
        testRMSE = []
        testCorr = []

        if False:
            # Train decision tree with varying leaf size
            for leaf_size in range(trainX.shape[0]):
                print(leaf_size + 1)
                learner = bl.BagLearner(learner=dt.DTLearner,kwargs={'leaf_size': leaf_size+1})  # create a LinRegLearner

                learner.addEvidence(trainX, trainY)  # train it

                # evaluate in sample
                predY = learner.query(trainX)  # get the predictions
                trainRMSE.append(math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0]))
                trainCorr.append(np.corrcoef(np.squeeze(predY), y=trainY)[0, 1])

                # evaluate out of sample
                predY = learner.query(testX)  # get the predictions
                testRMSE.append(math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0]))
                testCorr.append(np.corrcoef(np.squeeze(predY), y=testY)[0, 1])

            np.savetxt("/home/rll8/assess_learners/Report Data/BagLearner.csv",
                       np.stack(
                           (np.array(trainRMSE), np.array(trainCorr), np.array(testRMSE), np.array(testCorr))).transpose(),
                       fmt='%.8f', delimiter=',')

        trainRMSE_DT = []
        trainTime_DT = []
        testRMSE_DT = []
        trainRMSE_RT = []
        trainTime_RT = []
        testRMSE_RT = []

        if False:
            # Train single decision and random tree
            for leaf_size in range(trainX.shape[0]):
                print(leaf_size + 1)
                learnerDT = dt.DTLearner(leaf_size=leaf_size + 1)  # create a LinRegLearner

                tic = time.time()
                learnerDT.addEvidence(trainX, trainY)  # train it
                toc = time.time()

                trainTime_DT.append(toc - tic)

                # evaluate in sample
                predY = learnerDT.query(trainX)  # get the predictions
                trainRMSE_DT.append(math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0]))

                # evaluate out of sample
                predY = learnerDT.query(testX)  # get the predictions
                testRMSE_DT.append(math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0]))

                learnerRT = rt.RTLearner(leaf_size=leaf_size + 1)  # create a LinRegLearner

                tic = time.time()
                learnerRT.addEvidence(trainX, trainY)  # train it
                toc = time.time()

                trainTime_RT.append(toc - tic)

                # evaluate in sample
                predY = learnerRT.query(trainX)  # get the predictions
                trainRMSE_RT.append(math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0]))

                # evaluate out of sample
                predY = learnerRT.query(testX)  # get the predictions
                testRMSE_RT.append(math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0]))

            np.savetxt("/home/rll8/assess_learners/Report Data/DTLearner_RTLearner.csv",
                       np.stack(
                           (np.array(trainRMSE_DT), np.array(testRMSE_DT), np.array(trainTime_DT),
                            np.array(trainRMSE_RT), np.array(testRMSE_RT), np.array(trainTime_RT))).transpose(),
                       fmt='%.8f', delimiter=',')

        trainRMSE_DT = []
        trainTime_DT = []
        testRMSE_DT = []
        trainRMSE_RT = []
        trainTime_RT = []
        testRMSE_RT = []

        if False:
            # Train trees in bagging
            for leaf_size in range(trainX.shape[0]):
                print(leaf_size + 1)
                learnerDT = bl.BagLearner(learner=dt.DTLearner,kwargs={'leaf_size':leaf_size + 1})  # create a LinRegLearner

                tic = time.time()
                learnerDT.addEvidence(trainX, trainY)  # train it
                toc = time.time()

                trainTime_DT.append(toc - tic)

                # evaluate in sample
                predY = learnerDT.query(trainX)  # get the predictions
                trainRMSE_DT.append(math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0]))

                # evaluate out of sample
                predY = learnerDT.query(testX)  # get the predictions
                testRMSE_DT.append(math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0]))

                learnerRT = bl.BagLearner(learner=rt.RTLearner,kwargs={'leaf_size':leaf_size + 1}) # create a LinRegLearner

                tic = time.time()
                learnerRT.addEvidence(trainX, trainY)  # train it
                toc = time.time()

                trainTime_RT.append(toc - tic)

                # evaluate in sample
                predY = learnerRT.query(trainX)  # get the predictions
                trainRMSE_RT.append(math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0]))

                # evaluate out of sample
                predY = learnerRT.query(testX)  # get the predictions
                testRMSE_RT.append(math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0]))

            np.savetxt("/home/rll8/assess_learners/Report Data/BagLearner_DTLearner_RTLearner.csv",
                       np.stack(
                           (np.array(trainRMSE_DT), np.array(testRMSE_DT), np.array(trainTime_DT),
                            np.array(trainRMSE_RT), np.array(testRMSE_RT), np.array(trainTime_RT))).transpose(),
                       fmt='%.8f', delimiter=',')

        trainRMSE_RT_ALL = np.zeros((32,10))
        trainTime_RT_ALL = np.zeros((32,10))
        testRMSE_RT_ALL = np.zeros((32,10))

        if True:
            seeds = range(1,11)
            # Train different random trees
            for idx, seed in enumerate(seeds):
                trainRMSE_RT = []
                trainTime_RT = []
                testRMSE_RT = []
                for leaf_size in range(32):
                    print(leaf_size + 1)
                    np.random.seed(seed)

                    learnerRT = rt.RTLearner(leaf_size=leaf_size + 1) # create a LinRegLearner

                    tic = time.time()
                    learnerRT.addEvidence(trainX, trainY)  # train it
                    toc = time.time()

                    trainTime_RT.append(toc - tic)

                    # evaluate in sample
                    predY = learnerRT.query(trainX)  # get the predictions
                    trainRMSE_RT.append(math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0]))

                    # evaluate out of sample
                    predY = learnerRT.query(testX)  # get the predictions
                    testRMSE_RT.append(math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0]))
                trainRMSE_RT_ALL[:, idx] = trainRMSE_RT
                trainTime_RT_ALL[:, idx] = trainTime_RT
                testRMSE_RT_ALL[:, idx] = testRMSE_RT

            np.savetxt("/home/rll8/assess_learners/Report Data/Random_RTLearner_trainRMSE.csv",trainRMSE_RT_ALL,
                       fmt='%.8f', delimiter=',')
            np.savetxt("/home/rll8/assess_learners/Report Data/Random_RTLearner_trainTime.csv", trainTime_RT_ALL,
                       fmt='%.8f', delimiter=',')
            np.savetxt("/home/rll8/assess_learners/Report Data/Random_RTLearner_testRMSE.csv", testRMSE_RT_ALL,
                       fmt='%.8f', delimiter=',')