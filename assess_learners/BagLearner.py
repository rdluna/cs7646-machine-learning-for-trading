import numpy as np
import pandas as pd
import DTLearner as dt

class BagLearner(object):
    def __init__(self, learner, kwargs={}, bags=20, boost=False, verbose=False):
        self.learner = learner
        self.kwargs = kwargs
        self.bags = bags
        self.boost = boost
        self.verbose = verbose

    def author(self):
        return 'rll8'

    def addEvidence(self, Xtrain, Ytrain):
        self.Xtrain = Xtrain
        self.Ytrain = Ytrain
        self.learners = []
        N = self.Xtrain.shape[0]
        for bagNumber in range(self.bags):
            learner = self.learner(**self.kwargs)
            bagIdx = np.random.randint(0, N, N)
            Xtrain = self.Xtrain[bagIdx, :]
            Ytrain = self.Ytrain[bagIdx]
            learner.addEvidence(Xtrain, Ytrain)
            self.learners.append(learner)

    def query(self, Xtest):
        self.Ytest = []
        for learner in self.learners:
            Ytest = learner.query(Xtest)
            self.Ytest.append(Ytest)
        return np.mean(self.Ytest, axis=0)


if __name__ == "__main__":
    data = pd.read_csv("/home/rll8/assess_learners/Data/Istanbul.csv", index_col=0)
    Xtrain = data.values[:, 0:-1]
    Ytrain = data.values[:, -1]
    learner = BagLearner(learner=dt.DTLearner, kwargs={'leaf_size': 5})
    learner.addEvidence(Xtrain, Ytrain)
    Y = learner.query(Xtrain)