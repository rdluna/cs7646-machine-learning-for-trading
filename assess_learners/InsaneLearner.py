import numpy as np
import BagLearner as bl, LinRegLearner as lrl
class InsaneLearner(object):
    def __init__(self, verbose=False):
        self.verbose = verbose
    def author(self):
        return 'rll8'
    def addEvidence(self, Xtrain, Ytrain):
        self.learners = []
        for i in range(20):
            learner = bl.BagLearner(learner=lrl.LinRegLearner)
            learner.addEvidence(Xtrain, Ytrain)
            self.learners.append(learner)
    def query(self, Xtest):
        self.Ytest = []
        for learner in self.learners:
            self.Ytest.append(learner.query(Xtest))
        return np.mean(self.Ytest, axis=0)
