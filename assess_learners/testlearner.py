"""
Test a learner.  (c) 2015 Tucker Balch
"""

import numpy as np
import math
import DTLearner as dt
import RTLearner as rt
import BagLearner as bl
import InsaneLearner as it
import pandas as pd
import time

if __name__=="__main__":

    files = ['3_groups.csv', 'Istanbul.csv', 'ripple.csv', 'simple.csv', 'winequality-red.csv','winequality-white.csv']

    for file in files:
        if file != "Istanbul.csv":
            data = pd.read_csv("/home/rll8/assess_learners/Data/" + file).values
        else:
            data = pd.read_csv("/home/rll8/assess_learners/Data/" + file, index_col=0).values

        print("===============================================")
        print(file)

        # compute how much of the data is training and testing
        train_rows = int(0.6* data.shape[0])
        test_rows = data.shape[0] - train_rows

        # separate out training and testing data
        trainX = data[:train_rows,0:-1]
        trainY = data[:train_rows,-1]
        testX = data[train_rows:,0:-1]
        testY = data[train_rows:,-1]

        print testX.shape
        print testY.shape

        # create a learner and train it
        learner = it.InsaneLearner() # create a LinRegLearner

        tic = time.time()
        learner.addEvidence(trainX, trainY) # train it
        toc = time.time()

        print("Training time: " + str(toc-tic))

        print learner.author()

        # evaluate in sample
        predY = learner.query(trainX) # get the predictions
        rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
        print
        print "In sample results"
        print "RMSE: ", rmse
        c = np.corrcoef(np.squeeze(predY), y=trainY)
        print "corr: ", c[0,1]

        # evaluate out of sample
        predY = learner.query(testX) # get the predictions
        rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
        print
        print "Out of sample results"
        print "RMSE: ", rmse
        c = np.corrcoef(np.squeeze(predY), y=testY)
        print "corr: ", c[0,1]
        print("-----------------------------------------------")