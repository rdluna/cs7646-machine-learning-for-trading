"""MC1-P2: Optimize a portfolio.

Copyright 2017, Georgia Tech Research Corporation
Atlanta, Georgia 30332-0415
All Rights Reserved
"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import datetime as dt
import scipy.optimize as opt
from util import get_data

def compute_portfolio_value(prices, allocs, sv):
    # Normalize the prices
    norm_prices = prices / prices.iloc[0]
    # Multiply by the allocations
    alloc_prices = norm_prices * allocs
    # Multiply by the starting value to get the cash on each asset over time
    position_values = alloc_prices * sv
    # Calculate the portfolio value
    portfolio_value = np.sum(position_values, axis=1)
    return portfolio_value

def compute_portfolio_stats(prices, allocs, rfr=0.0, sf=252.0, sv=1):
    # Calculate the portfolio value
    portfolio_value = compute_portfolio_value(prices, allocs, sv)
    # Calculate the daily returns
    dr = (portfolio_value / portfolio_value.shift(1)) - 1
    # Calculate the average daily return
    adr = dr.mean()
    # Calculate the cumulative returns
    cr = (portfolio_value[-1] / portfolio_value[0]) - 1
    # Calculate the standard deviation of the daily returns
    sddr = dr.std()
    # Calculate the Sharpe ratio
    sr = np.sqrt(sf) * (adr - rfr)/sddr
    return cr, adr, sddr, sr

def minimize_fun(allocs, prices):
    cr, adr, sddr, sr = compute_portfolio_stats(prices, allocs)
    return sddr

def find_optimal_allocations(prices):
    starting_values = np.ones((prices.shape[1],1))/prices.shape[1]
    bounds = [(0,1) for _ in range(prices.shape[1])]
    constraints = ({'type': 'eq', 'fun': lambda inputs: np.sum(inputs) - 1})
    opt_out = opt.minimize(minimize_fun, x0=starting_values, args=(prices), bounds=bounds, constraints=constraints)
    best_allocs = opt_out['x']
    best_allocs[np.isnan(best_allocs)] = 0
    return best_allocs

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def optimize_portfolio(sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), \
    syms=['GOOG','AAPL','GLD','XOM'], gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # find the allocations for the optimal portfolio
    # note that the values here ARE NOT meant to be correct for a test case
    allocs = find_optimal_allocations(prices) # add code here to find the allocations
    cr, adr, sddr, sr = compute_portfolio_stats(prices, allocs) # add code here to compute stats
    port_val = compute_portfolio_value(prices, allocs, 1) # add code here to compute daily portfolio values

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        mpl.rcParams['axes.color_cycle'] = ['g', 'b']
        norm_SPY = prices_SPY / prices_SPY.iloc[0]
        df_temp = pd.concat([port_val, norm_SPY], keys=['Portfolio', 'SPY'], axis=1)
        plt.plot_date(prices.index.values, df_temp, linestyle="solid", marker="None", fmt='')
        plt.title("Daily Portfolio Value and SPY", fontsize=12)
        plt.xlabel("Date")
        plt.ylabel("Price")
        plt.ylim((np.floor(df_temp.min().min()*10)/10, np.ceil(df_temp.max().max()*10)/10))
        plt.legend(["Portfolio", "SPY"])
        plt.grid(linestyle='dashed')
        plt.savefig("/home/rll8/optimize_something/plot.pdf")

    return allocs, cr, adr, sddr, sr

def test_code():
    # This function WILL NOT be called by the auto grader
    # Do not assume that any variables defined here are available to your function/code
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!

    start_date = dt.datetime(2008,6,1)
    end_date = dt.datetime(2009,6,1)
    symbols = ['IBM','X','GLD']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = True)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    # This code WILL NOT be called by the auto grader
    # Do not assume that it will be called
    test_code()
