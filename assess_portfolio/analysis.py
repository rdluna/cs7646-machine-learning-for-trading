"""Analyze a portfolio.

Copyright 2017, Georgia Tech Research Corporation
Atlanta, Georgia 30332-0415
All Rights Reserved
"""

import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
from util import get_data, plot_data


def compute_portfolio_value(prices, allocs, value):
    # Normalize the prices
    norm_prices = prices / prices.iloc[0]
    # Multiply by the allocations
    alloc_prices = norm_prices * allocs
    # Multiply by the starting value to get the cash on each asset over time
    position_values = alloc_prices * value
    # Calculate the portfolio value
    portfolio_value = np.sum(position_values, axis=1)
    return portfolio_value


def compute_portfolio_stats(prices, allocs, rfr, sf, sv):
    # Calculate the portfolio value
    portfolio_value = compute_portfolio_value(prices, allocs, sv)
    # Calculate the daily returns
    dr = (portfolio_value / portfolio_value.shift(1)) - 1
    # Calculate the average daily return
    adr = dr.mean()
    # Calculate the cumulative returns
    cr = (portfolio_value[-1] / portfolio_value[0]) - 1
    # Calculate the standard deviation of the daily returns
    sddr = dr.std()
    # Calculate the Sharpe ratio
    sr = np.sqrt(sf) * (adr - rfr)/sddr
    return cr, adr, sddr, sr

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def assess_portfolio(sd=dt.datetime(2008,1,1),
                     ed=dt.datetime(2009,1,1),
                     syms=['GOOG','AAPL','GLD','XOM'],
                     allocs=[0.1,0.2,0.3,0.4],
                     sv=1000000, rfr=0.0, sf=252.0,
                     gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices.fillna(method="ffill", inplace=True)
    prices.fillna(method="bfill", inplace=True)
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # Get daily portfolio value
    port_val = compute_portfolio_value(prices, allocs, sv)

    # Get portfolio statistics (note: std_daily_ret = volatility)
    cr, adr, sddr, sr = compute_portfolio_stats(prices=prices, allocs=allocs, rfr=rfr, sf=sf, sv=sv)

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        norm_SPY = prices_SPY / prices_SPY.iloc[0]
        plt.plot_date(prices.index.values, port_val/sv, label="Porfolio", linestyle="solid", marker="None", color="blue")
        plt.plot_date(prices.index.values, norm_SPY, label='SPY', linestyle="solid", marker="None", color="green")
        plt.grid()
        plt.legend()
        plt.title('Daily porfolio value and SPY')
        plt.xlabel('Date')
        plt.ylabel('Normalized price')
        plt.savefig("/home/rll8/assess_portfolio/plot")

    # Add code here to properly compute end value
    # ev = port_val[-1]
    ev = (1 + cr) * sv

    return cr, adr, sddr, sr, ev

def test_code():
    # This code WILL NOT be tested by the auto grader
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!
    start_date = dt.datetime(2010,6,1)
    end_date = dt.datetime(2010,12,31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']
    allocations = [0.2, 0.3, 0.4, 0.1]
    start_val = 1000000  
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        rfr=risk_free_rate, \
        sf=sample_freq, \
        gen_plot = True)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    test_code()
